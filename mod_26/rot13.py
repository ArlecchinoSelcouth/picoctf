with open("flag", "r") as file:
    encrypted_flag = file.readline().strip()
    flag = ""

    rot13 = str.maketrans(
    'ABCDEFGHIJKLMabcdefghijklmNOPQRSTUVWXYZnopqrstuvwxyz',
    'NOPQRSTUVWXYZnopqrstuvwxyzABCDEFGHIJKLMabcdefghijklm')
    flag = encrypted_flag.translate(rot13)

print("Encrypted flag: {flag_e}\nDecrypted flag: {flag_d}".format(
    flag_e=encrypted_flag,
    flag_d=flag
))