import sys

data = sys.stdin.read()
flag = ""
curr_char = ""
for d in data:
    if d.strip():
        curr_char += d
    else:
        if curr_char.strip():
            flag += chr(int(curr_char.strip()))
        curr_char = ""

print(flag)