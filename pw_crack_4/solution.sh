function solve() {

    for pass in $(cat level4.py | grep pos_pw_list | sed 's/pos_pw_list = //' | sed 's/\[*\]*,*"*//g') 
    do
        echo $pass > pass.txt
        python3 level4.py < pass.txt
    done
}

solve | grep picoCTF
