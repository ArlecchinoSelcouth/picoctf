function solve() {
    for pass in $(cat dictionary.txt)
    do
        echo $pass > pass.txt
        python3 level5.py < pass.txt
    done
}

solve | grep picoCTF
