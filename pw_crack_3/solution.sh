function solve() {

    for pass in $(cat level3.py | grep pos_pw_list | sed 's/pos_pw_list = //' | sed 's/\[*\]*,*"*//g') 
    do
        echo $pass > pass.txt
        python3 level3.py < pass.txt
    done
}

solve | grep picoCTF